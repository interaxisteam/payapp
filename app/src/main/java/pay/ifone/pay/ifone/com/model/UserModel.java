package pay.ifone.pay.ifone.com.model;

/**
 * Created by Interaxis on 27-01-2018.
 */
public class UserModel {
    private String username;
    private String password;
    private String hirerachy;

    public String getHirerachy() {
        return hirerachy;
    }

    public void setHirerachy(String hirerachy) {
        this.hirerachy = hirerachy;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }



}
