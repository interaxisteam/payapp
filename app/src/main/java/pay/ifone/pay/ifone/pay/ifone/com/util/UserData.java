package pay.ifone.pay.ifone.pay.ifone.com.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import pay.ifone.pay.ifone.com.model.UserModel;

/**
 * Created by Interaxis on 03-02-2018.
 */

public class UserData {
String PRE_NAME="fonepay";
    public void saveUserdata(UserModel userModel, Context context){
        SharedPreferences mPrefs = context
                .getSharedPreferences(PRE_NAME, 0);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(userModel);
        prefsEditor.clear().commit();
        prefsEditor.putString("USER",json);
        prefsEditor.commit();

    }

    public  UserModel getUser(Context context) {

        SharedPreferences mPrefs = context
                .getSharedPreferences(PRE_NAME, 0);
        Gson gson = new Gson();
        String json = mPrefs.getString("USER","");
        UserModel obj = gson.fromJson(json, UserModel.class);
        return obj;

    }
    public void clear(Context context){
        SharedPreferences mPrefs = context
                .getSharedPreferences(PRE_NAME, 0);
    }

}
