package pay.ifone.com.ifonepay;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageView;
        import android.widget.TextView;

        import java.util.ArrayList;

import pay.ifone.pay.ifone.com.model.HistoryModel;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private ArrayList<HistoryModel> dataSet;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView account;
        TextView dateTime;
        TextView amount;
        TextView type;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.account = (TextView) itemView.findViewById(R.id.account);
            this.amount = (TextView) itemView.findViewById(R.id.amount);
            this.type = (TextView) itemView.findViewById(R.id.type);
            this.dateTime = (TextView) itemView.findViewById(R.id.dateTime);
        }
    }

    public CustomAdapter(ArrayList<HistoryModel> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.history_card, parent, false);



        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView account=holder.account;
        TextView dateTime=holder.dateTime;
        TextView amount=holder.amount;
        TextView type=holder.type;
//if(listPosition==0) {
//    SpannableString spanString = new SpannableString(dataSet.get(listPosition).getAccount());
//    spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
//    account.setText(spanString);
//
//    SpannableString spanString2 = new SpannableString(dataSet.get(listPosition).getDateTime());
//    spanString2.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString2.length(), 0);
//    dateTime.setText(spanString2);
//
//    SpannableString spanString3 = new SpannableString(dataSet.get(listPosition).getAmount());
//    spanString3.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString3.length(), 0);
//    amount.setText(spanString3);
//
//    SpannableString spanString4 = new SpannableString(dataSet.get(listPosition).getType());
//    spanString4.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString4.length(), 0);
//    type.setText(spanString4);
//
//
//}else{
    account.setText(dataSet.get(listPosition).getAccount());
    dateTime.setText(dataSet.get(listPosition).getDateTime());
    amount.setText(dataSet.get(listPosition).getAmount());
    type.setText(dataSet.get(listPosition).getType());
//}

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}