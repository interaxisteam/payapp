package pay.ifone.com.ifonepay;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.gordonwong.materialsheetfab.MaterialSheetFab;
import com.gordonwong.materialsheetfab.MaterialSheetFabEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import pay.ifone.pay.ifone.com.model.UserModel;
import pay.ifone.pay.ifone.pay.ifone.com.util.AppController;
import pay.ifone.pay.ifone.pay.ifone.com.util.UserData;
import pay.ifone.pay.ifone.pay.ifone.com.util.Util;


public class MainActivity extends AppCompatActivity  {
TextView balance;
UserData userData;
UserModel userModel;
CheckBox checkBox;
boolean netinfo;
TextView version;
    MaterialSheetFab materialSheetFab;
    private int statusBarColor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
MainFragment mainFragment= new MainFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.mainFrame, mainFragment).commit();
netinfo=new NetworkChecking().getNetInfo(getApplicationContext());
        if(!netinfo){
            Toast.makeText(getApplicationContext(),"No Network Connectivity",Toast.LENGTH_LONG).show();
        }
balance=(TextView)findViewById(R.id.txtBalance) ;
version=(TextView)findViewById(R.id.txtVersion);
version.setText("Version: "+BuildConfig.VERSION_NAME);


        Fab fab = (Fab) findViewById(R.id.fab);

        View sheetView = findViewById(R.id.fab_sheet);
        View overlay = findViewById(R.id.overlay);
        int sheetColor = getResources().getColor(R.color.colorGreen);
        int fabColor = getResources().getColor(R.color.common_signin_btn_text_light);
        // Initialize material sheet FAB

        materialSheetFab = new MaterialSheetFab<>(fab, sheetView, overlay,
                sheetColor, fabColor);
        materialSheetFab.setEventListener(new MaterialSheetFabEventListener() {
            @Override
            public void onShowSheet() {
                // Save current status bar color
//                statusBarColor = getStatusBarColor();
//                // Set darker status bar color to match the dim overlay
//                setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            }

            @Override
            public void onHideSheet() {
                // Restore status bar color
//                setStatusBarColor(statusBarColor);
            }
        });
    }

    private int getStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return getWindow().getStatusBarColor();
        }
        return 0;
    }

    private void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(color);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
// Handle action bar item clicks here. The action bar will
// automatically handle clicks on the Home/Up button, so long
// as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent= new Intent(MainActivity.this,LoginActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onResume() {
        userData= new UserData();
        userModel=userData.getUser(getApplicationContext());
        getBalance(userModel.getUsername());
        super.onResume();
    }
public void editUser(View view){
Intent intent= new Intent(MainActivity.this,LoginActivity.class);
intent.putExtra("edit","edit");
startActivity(intent);
    if (materialSheetFab.isSheetVisible()) {
        materialSheetFab.hideSheet();
    }
}

public void feedback(View v){


    if (ContextCompat.checkSelfPermission(MainActivity.this,
            Manifest.permission.GET_ACCOUNTS)
            != PackageManager.PERMISSION_GRANTED) {

        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                Manifest.permission.GET_ACCOUNTS)) {

            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.

        } else {

            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.GET_ACCOUNTS},
                    243);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
    }else{
        if(!netinfo){
            Toast.makeText(getApplicationContext(),"No Network Connectivity",Toast.LENGTH_LONG).show();
        }else {
            FeedbackFragment feedbackFragment = new FeedbackFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.mainFrame, feedbackFragment).commit();
        }
    }
    if (materialSheetFab.isSheetVisible()) {
        materialSheetFab.hideSheet();
    }
}
    public void getBalance(String uid){
        Log.d("BALANCE","Inside balance");
        String url= Util.bal+uid;
    JsonObjectRequest jsObjRequest = new JsonObjectRequest
            (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
//                            mEmailView.setText("Response: " + response.toString());

                    Log.d("Balance", response.toString());
                    String bal="";
                    String status="";

                    try {
                        status = response.getString("status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        bal = response.getString("Balance");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if(status.equals("success")){
                        balance.setText("Balance: $"+bal);
                    }else{


                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO Auto-generated method stub
                    Log.d("Login", error.getMessage());

                }
            });
        AppController.getInstance().addToRequestQueue(jsObjRequest, "Balance");
}

public void fundTransfer(View v) {
    if (!netinfo) {
        Toast.makeText(getApplicationContext(), "No Network Connectivity", Toast.LENGTH_LONG).show();
    } else{
        FundTransferFragment fundTransferFragment = new FundTransferFragment();
    getSupportFragmentManager().beginTransaction()
            .replace(R.id.mainFrame, fundTransferFragment).commit();
}
}

    public void history(View v){
        if(!netinfo){
            Toast.makeText(getApplicationContext(),"No Network Connectivity",Toast.LENGTH_LONG).show();
        }else {
            HistoryFragment historyFragment = new HistoryFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.mainFrame, historyFragment).commit();
        }

    }
    void openWhatsappContact(String number) {
        PackageManager pm=getPackageManager();
      try {
        PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
        Uri uri = Uri.parse("smsto:" + number);
        Intent i = new Intent(Intent.ACTION_SEND);
          i.setType("text/plain");
        i.setPackage("com.whatsapp");
        i.putExtra(Intent.EXTRA_TEXT, "Please Send me new Pins Under *hierarchy: "+userModel.getHirerachy()+"*");
        startActivity(Intent.createChooser(i, "Share with"));

    }catch (PackageManager.NameNotFoundException e) {
        Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                .show();
    }

}
@SuppressLint("NewApi")
public void pinRequest(View v){
    if(!netinfo){
        Toast.makeText(getApplicationContext(),"No Network Connectivity",Toast.LENGTH_LONG).show();
    }else {


        CustomDialogClass customDialog2= new CustomDialogClass(this) {

            @Override
            public void onClick(View view) {
                Button button= (Button)view;
//                                    Toast.makeText(getActivity(),"Clicked "+button.getText().toString(),Toast.LENGTH_SHORT).show();

                String txt=button.getText().toString();
                if(txt.toLowerCase().contains("whatsapp")){
                    openWhatsappContact("00919995526660");
                    dismiss();
                }
                if(txt.toLowerCase().contains("email")){
                    sendMail();
                    dismiss();
                }

            }
        };
        customDialog2.create();
//        customDialog2.no.setVisibility(View.GONE);
        customDialog2.yes.setText("WhatsApp");
        customDialog2.no.setText("EMail");
        customDialog2.setMessage("Please choose request mode".toUpperCase());
        customDialog2.show();
    }
}

public void sendMail(){
    Intent i = new Intent(Intent.ACTION_SEND);
    i.setType("message/rfc822");



    i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"sales@a2zcomm.com"});
    i.putExtra(Intent.EXTRA_SUBJECT, "New Pin Request Via App");
    i.putExtra(Intent.EXTRA_TEXT   , "Please send me new pins under *"+userModel.getHirerachy()+"*");
    try {
        startActivity(i);
    } catch (android.content.ActivityNotFoundException ex) {
        Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
    }
}

    @Override
    public void onBackPressed() {
        getBalance(userModel.getUsername());
        android.support.v4.app.Fragment f = getSupportFragmentManager().findFragmentById(R.id.mainFrame);
        Log.d("cur frag",f.getClass().getName());
        if (materialSheetFab.isSheetVisible()) {
            materialSheetFab.hideSheet();
        }else {
            if (f.getClass().getName().contains("Main")) {
                // On back button, or popBackStack(),
                // the fragment that's becoming visible executes here,
                // but not the one being popped, or others on the back stack
                super.onBackPressed();
                // So, for my case, I can change action bar bg color per fragment
            } else {

                MainFragment mainFragment = new MainFragment();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.mainFrame, mainFragment).commit();
            }
        }
        //
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 243: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    FeedbackFragment feedbackFragment = new FeedbackFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.mainFrame, feedbackFragment).commit();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }
    }

}
