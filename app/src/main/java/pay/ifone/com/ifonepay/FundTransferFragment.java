package pay.ifone.com.ifonepay;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import pay.ifone.pay.ifone.com.model.UserModel;
import pay.ifone.pay.ifone.pay.ifone.com.util.AppController;
import pay.ifone.pay.ifone.pay.ifone.com.util.UserData;
import pay.ifone.pay.ifone.pay.ifone.com.util.Util;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FundTransferFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FundTransferFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FundTransferFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private EditText txtSubscriber;
    private EditText txtConfirmSubscriber;
    private EditText txtAmount;
    private Button btnTransfer;
    private View mProgressView;
    public FundTransferFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FundTransferFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FundTransferFragment newInstance(String param1, String param2) {
        FundTransferFragment fragment = new FundTransferFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fund_transfer, container, false);
    }
    String owner="";
    @SuppressLint("NewApi")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
                super.onViewCreated(view, savedInstanceState);
                txtSubscriber=(EditText)view.findViewById(R.id.txtSubscriber);
                txtConfirmSubscriber=(EditText)view.findViewById(R.id.txtSubscriberConfirm);
                txtAmount=(EditText)view.findViewById(R.id.txtAmount);
                btnTransfer=(Button)view.findViewById(R.id.buttonTransfer);
        mProgressView = view.findViewById(R.id.progressBar);
                txtConfirmSubscriber.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

                    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                        return false;
                    }

                    public void onDestroyActionMode(ActionMode mode) {
                    }

                    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                        return false;
                    }

                    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                        return false;
                    }
                });

                txtAmount.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

                    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                        return false;
                    }

                    public void onDestroyActionMode(ActionMode mode) {
                    }

                    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                        return false;
                    }

                    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                        return false;
                    }
                });


                btnTransfer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(txtSubscriber.getText().toString().trim().length()>=3) {
                            final String subscriber=txtConfirmSubscriber.getText().toString().trim();
                            if (subscriber.equals(txtSubscriber.getText().toString().trim())) {
                                    final String amount= txtAmount.getText().toString().trim();
                                    if(amount.contains(".") || amount.trim().equals("")){
                                        txtAmount.setError("Please enter a valid amount");
                                        txtAmount.requestFocus();
                                }else{
                                        showProgress(true);
                                        UserData userData= new UserData();
                                        final UserModel userModel= userData.getUser(getActivity());
                                        String url= Util.url2+userModel.getUsername();
                                        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                                                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                                                    @Override
                                                    public void onResponse(JSONObject response) {
        //                            mEmailView.setText("Response: " + response.toString());

                                                        Log.d("Response",response.toString());

                                                        String status="";

                                                        try {
                                                            status = response.getString("status");
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                        FundTransferFragment.this.owner="";



                                                        JSONArray jsonArray;
                                                        if(status.equals("success")){
                                                            try {
                                                                jsonArray = response.getJSONArray("records");
                                                                JSONObject jsonObject=jsonArray.getJSONObject(0);

                                                                owner=jsonObject.getString("UserOwner");
                                                                Log.d("Reseller",owner+"****");

                                                                String url= Util.url2+subscriber;
                                                                JsonObjectRequest jsObjRequest = new JsonObjectRequest
                                                                        (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                                                                            @Override
                                                                            public void onResponse(JSONObject response) {
        //                            mEmailView.setText("Response: " + response.toString());

                                                                                Log.d("Response",response.toString());

                                                                                String status="";

                                                                                try {
                                                                                    status = response.getString("status");
                                                                                } catch (JSONException e) {
                                                                                    e.printStackTrace();
                                                                                }
                                                                                String owner2="";



                                                                                JSONArray jsonArray;
                                                                                if(status.equals("success")){
                                                                                    try {
                                                                                        jsonArray = response.getJSONArray("records");
                                                                                        JSONObject jsonObject=jsonArray.getJSONObject(0);

                                                                                        owner2=jsonObject.getString("UserOwner");
                                                                                        Log.d("Reseller2",owner2+"****");
                                                                                        if(owner.equals(owner2)){
                                                                                            showProgress(false);

                                                                                            CustomDialogClass customDialog= new CustomDialogClass(getActivity()) {

                                                                                                @Override
                                                                                                public void onClick(View view) {
                                                                                                    Button button= (Button)view;
//                                                                                                    Toast.makeText(getActivity(),"Clicked "+button.getText().toString(),Toast.LENGTH_SHORT).show();

                                                                                                    String txt=button.getText().toString();
                                                                                                    if(txt.toLowerCase().contains("no")){
                                                                                                        dismiss();
                                                                                                    }
                                                                                                    if(txt.toLowerCase().contains("yes")){
                                                                                                        showProgress(true);
                                                                                                        FundTransferFragment.this.transfer(userModel.getUsername(),userModel.getPassword(),subscriber,amount);
                                                                                                        dismiss();
                                                                                                    }
                                                                                                }
                                                                                            };
                                                                                            customDialog.create();
                                                                                            customDialog.setMessage("You sure want to transfer $"+amount+" to "+subscriber);
                                                                                            customDialog.show();
                                                                                        }else{
                                                                                            showProgress(false);
                                                                                            Toast.makeText(getActivity(), "Enter a valid subscriber ID", Toast.LENGTH_SHORT).show();
                                                                                        }

                                                                                    } catch (JSONException e) {
                                                                                        e.printStackTrace();
                                                                                    }


                                                                                }else{
                                                                                    showProgress(false);
                                                                                    Toast.makeText(getActivity(), "Enter a valid subscriber ID", Toast.LENGTH_SHORT).show();
                                                                                }


                                                                            }
                                                                        }, new Response.ErrorListener() {

                                                                            @Override
                                                                            public void onErrorResponse(VolleyError error) {
                                                                                // TODO Auto-generated method stub


                                                                            }
                                                                        });

                                                                AppController.getInstance().addToRequestQueue(jsObjRequest, "UserOwner");


                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }


                                                        }else{

                                                        }


                                                    }
                                                }, new Response.ErrorListener() {

                                                    @Override
                                                    public void onErrorResponse(VolleyError error) {
                                                        // TODO Auto-generated method stub


                                                    }
                                                });

                                        AppController.getInstance().addToRequestQueue(jsObjRequest, "UserOwner");



                                }
                            } else {
                                txtConfirmSubscriber.setError("Subscriber ids not match");
                                txtConfirmSubscriber.requestFocus();
                            }
                        }else{
                            txtSubscriber.setError("Enter a valid subscriber Id");
                            txtSubscriber.requestFocus();
                        }
                    }
                });

    }


    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

//            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
//            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
//                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
//                @Override
//                public void onAnimationEnd(Animator animation) {
//                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
//                }
//            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
//            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public void transfer(final String pin, String password, String subscriber, String amount){


        String url= Util.trans+pin+"&password="+password+"&to="+subscriber+"&amount="+amount+"&userId="+pin;
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @SuppressLint("NewApi")
                    @Override
                    public void onResponse(JSONObject response) {
//                            mEmailView.setText("Response: " + response.toString());
showProgress(false);
                        ((MainActivity)getActivity()).getBalance(pin);
                        Log.d("Response",response.toString());



                        String status="";

                        try {
                            status = response.getString("status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        String message="";
                        try {
                            message = response.getString("message");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                        if(status.equals("success")){
                            String newBalance="";
                            try {
                                newBalance = response.getString("Updated_Balance");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            CustomDialogClass customDialog2= new CustomDialogClass(getActivity()) {

                                @Override
                                public void onClick(View view) {
                                    Button button= (Button)view;
//                                    Toast.makeText(getActivity(),"Clicked "+button.getText().toString(),Toast.LENGTH_SHORT).show();

                                    String txt=button.getText().toString();
                                    if(txt.toLowerCase().contains("ok")){
                                        dismiss();
                                    }

                                }
                            };
                            customDialog2.create();
                            customDialog2.no.setVisibility(View.GONE);
                            customDialog2.yes.setText("OK");
                            customDialog2.setMessage("Transfer was successfull and your new balance is $"+newBalance);
                            customDialog2.show();
                        }else{

                            CustomDialogClass customDialog2= new CustomDialogClass(getActivity()) {

                                @Override
                                public void onClick(View view) {
                                    Button button= (Button)view;
//                                    Toast.makeText(getActivity(),"Clicked "+button.getText().toString(),Toast.LENGTH_SHORT).show();

                                    String txt=button.getText().toString();
                                    if(txt.toLowerCase().contains("ok")){
                                        dismiss();
                                    }

                                }
                            };
                            customDialog2.create();
                            customDialog2.no.setVisibility(View.GONE);
                            customDialog2.yes.setText("OK");
                            customDialog2.setMessage("Failed "+message);
                            customDialog2.show();

                        }


                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub


                    }
                });

        AppController.getInstance().addToRequestQueue(jsObjRequest, "UserOwner");

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
