package pay.ifone.com.ifonepay;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import pay.ifone.pay.ifone.com.model.HistoryModel;
import pay.ifone.pay.ifone.com.model.UserModel;
import pay.ifone.pay.ifone.pay.ifone.com.util.AppController;
import pay.ifone.pay.ifone.pay.ifone.com.util.UserData;
import pay.ifone.pay.ifone.pay.ifone.com.util.Util;

/**
 * Created by Interaxis on 12-02-2018.
 */

public class HistoryFragment extends Fragment {

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<HistoryModel> data;
    static View.OnClickListener myOnClickListener;
    private static ArrayList<Integer> removedItems;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.history_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        UserModel userModel= new UserData().getUser(getActivity());

        SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-M-dd");

        String url= Util.history+"subscriberId="+userModel.getUsername()+"&from=2012-9-7%2000:00:00&to="+dateFormat.format(new Date())+"%2000:00:00&password="+userModel.getPassword()+"&userId="+userModel.getUsername();
        Log.d("History", url);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
//                            mEmailView.setText("Response: " + response.toString());
data= new ArrayList<>();
                        Log.d("History", response.toString());
                        String bal="";
                        String status="";

                        try {
                            status = response.getString("status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        if(status.equals("success")){
                            JSONArray records=new JSONArray();
                            try {
                                records = response.getJSONArray("records");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//                            HistoryModel historyMod= new HistoryModel();
//                            historyMod.setType("Type");
//                            historyMod.setDateTime("Date/Time");
//                            historyMod.setAmount("Amount");
//                            historyMod.setAccount("Account");
//                            data.add(historyMod);
                            for (int i=0; i<records.length(); i++) {
                                try {
                                    JSONObject obj = records.getJSONObject(i);
                                    HistoryModel historyModel= new HistoryModel();
                                    historyModel.setType(obj.getString("Type"));
                                    if(historyModel.getType().equals("CREDIT")){
                                        historyModel.setAccount(obj.getString("From-Account"));
                                        historyModel.setType("CDT");
                                    }else {
                                        historyModel.setAccount(obj.getString("To-Account"));
                                        historyModel.setType("DBT");
                                    }
                                    historyModel.setAmount(obj.getString("Amount"));
                                    historyModel.setDateTime(obj.getString("Transferred-Date"));

                                    data.add(historyModel);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                            adapter = new CustomAdapter(data);
                            recyclerView.setAdapter(adapter);


                            Log.d("ARRAY SIZE",records.length()+" ****");
                        }else{


                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        Log.d("Login", error.getMessage());

                    }
                });
        AppController.getInstance().addToRequestQueue(jsObjRequest, "Balance");

//        for (int i = 0; i < MyData.nameArray.length; i++) {
//            data.add(new DataModel(
//                    MyData.nameArray[i],
//                    MyData.versionArray[i],
//                    MyData.id_[i],
//                    MyData.drawableArray[i]
//            ));
//        }

//        removedItems = new ArrayList<Integer>();


    }


}
