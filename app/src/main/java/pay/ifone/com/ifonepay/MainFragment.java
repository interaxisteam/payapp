package pay.ifone.com.ifonepay;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;




public class MainFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    List<TextView> indicatorViewList;
    private int[] pagerImageResource = {R.drawable.ui_back_cover};
    private ViewPager slidingPager;
    ViewPagerAdapter pageAdapter;
    LinearLayout indicatorLayout;
    private String mParam1;
    private String mParam2;


    public MainFragment() {
        // Required empty public constructor
    }

    public static MainFragment newInstance(String param1, String param2) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        return inflater.inflate(R.layout.content_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        indicatorViewList = new ArrayList<TextView>();
        slidingPager = (ViewPager)view.findViewById(R.id.sliding_pager);
        indicatorLayout = (LinearLayout)view.findViewById(R.id.indicatorLayout);

        for(int i=0;i<pagerImageResource.length;i++){
            createSlidingIndicator();}

        pageAdapter = new ViewPagerAdapter(getActivity().getApplicationContext(),pagerImageResource);
        slidingPager.setAdapter(pageAdapter);
        slidingPager.setCurrentItem(0);
        final Handler handler = new Handler();


        final Runnable update = new Runnable() {
            int currentImage=0;
            public void run() {
                if(currentImage==pagerImageResource.length){
                    currentImage = 0;
                }slidingPager.setCurrentItem(currentImage++);
            }
        };

        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 2000, 2000);
        slidingPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                for(int i=0; i<indicatorViewList.size();i++){
                    boolean enabled = false;
                    if(i==position){
                        enabled = true;
                    }
                    indicatorViewList.get(i).setEnabled(enabled);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }



    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    private void createSlidingIndicator(){
        int sideLength = dpToPx(8);

        int margin = dpToPx(2);
        TextView indicatorView = new TextView(getActivity());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(sideLength, sideLength);
        layoutParams.setMargins(margin,margin,margin,margin);
        indicatorView.setLayoutParams(layoutParams);
//        Log.d("api",Build.VERSION.SDK_INT+"   "+android.os.Build.VERSION_CODES.JELLY_BEAN);
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            indicatorView.setBackgroundDrawable( getResources().getDrawable(R.drawable.page_indicator) );
        } else {
            indicatorView.setBackground( getResources().getDrawable(R.drawable.page_indicator));
        }

        if(indicatorViewList.size()!=0){
            indicatorView.setEnabled(false);
        }
        indicatorLayout.addView(indicatorView);
        indicatorViewList.add(indicatorView);
    }
    // TODO: Rename method, update argument and hook method into UI event

}
