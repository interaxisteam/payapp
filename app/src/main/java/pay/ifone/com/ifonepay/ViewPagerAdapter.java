package pay.ifone.com.ifonepay;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;


public class ViewPagerAdapter extends PagerAdapter {
    Context context;
    int[] pagerImageResource;

    public ViewPagerAdapter(Context context, int[] pagerImageResource) {
        this.context = context;
        this.pagerImageResource = pagerImageResource;
    }

    @Override
    public int getCount() {
        return pagerImageResource.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.img_pager_item);
        Picasso.with(context).load(pagerImageResource[position]).into(imageView);
//        imageView.setImageResource(pagerImageResource[position]);

        container.addView(itemView);
        return itemView;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
