package pay.ifone.com.ifonepay;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import pay.ifone.mailsender.GMailSender;
import pay.ifone.pay.ifone.com.model.HistoryModel;
import pay.ifone.pay.ifone.com.model.UserModel;
import pay.ifone.pay.ifone.pay.ifone.com.util.AppController;
import pay.ifone.pay.ifone.pay.ifone.com.util.UserData;
import pay.ifone.pay.ifone.pay.ifone.com.util.Util;

/**
 * Created by Interaxis on 24-02-2018.
 */

public class FeedbackFragment extends Fragment {


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.feedback, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);


        final UserModel userModel= new UserData().getUser(getActivity());

        final EditText feedback=(EditText)view.findViewById(R.id.txtFeedback);
        final Button button= (Button)view.findViewById(R.id.feedButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
String eMail="";
                Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
                Account[] accounts = AccountManager.get(getActivity()).getAccounts();
                for (Account account : accounts) {
                    if (emailPattern.matcher(account.name).matches()) {
                        eMail = account.name;

                    }
                }
                String message= "Sender Email= "+eMail+ " Sender Pin= "+userModel.getUsername()+" hierarchy= "+userModel.getHirerachy()+ " \n";
                message= message+"\n "+feedback.getText();
                sendEmail(message);
                feedback.setText("");
                button.setEnabled(false);
            }
        });

//


    }


    public void sendEmail(final String message){
        new Thread(new Runnable() {

            public void run() {

                try {

                    GMailSender sender = new GMailSender(

                            "anil@interaxisnet.com",

                            "unwanded");



//                    sender.addAttachment(Environment.getExternalStorageDirectory().getPath()+"/image.jpg");

                    sender.sendMail("iFonePay FeedBack", message,

                            "anil@interaxisnet.com",

                            "sakeer@ifonegroup.com");


                    Toast.makeText(getActivity(),"Feedback Sent", Toast.LENGTH_SHORT).show();





                } catch (Exception e) {

//                    Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_LONG).show();



                }

            }

        }).start();

    }
}
